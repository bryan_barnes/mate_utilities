package com.naimuri.mate.utilities;

import java.io.FileInputStream;
import java.util.Properties;

/**
 *
 */

public class JavaProperties {

    public String getSpecifiedPropertyFromfile(String fileName, String key) throws Exception {
        FileInputStream file = new FileInputStream(fileName);
        Properties props = new Properties();
        props.load(file);

        String value = props.getProperty(key);
        return value;
    }

}
