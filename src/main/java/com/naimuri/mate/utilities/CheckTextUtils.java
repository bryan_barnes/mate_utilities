package com.naimuri.mate.utilities;


import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class CheckTextUtils {

    private static String nameOfLogger = "myLogger";
    private static Logger log = Logger.getLogger(nameOfLogger);

    public boolean checkStringAppearsInAnotherString(String text, String textToFind) {

        if (text.contains(textToFind)) {
            log.info("Pass: " + text + "contains " + textToFind);
            return true;
        } else {
            log.info("Fail: " + text + "... does not contain " + textToFind);
            return false;
        }

    }


    public String returnStringMatchingRegexPattern(String string, String patternToFind) throws Exception {

        Pattern pattern = Pattern.compile(patternToFind);
        Matcher matcher = pattern.matcher(string);

        if (matcher.find()) {
            log.info("Found " + matcher.group(0));
            return matcher.group(0);
        } else {
            log.info("Pattern not matched");
            return null;
        }

    }

}
