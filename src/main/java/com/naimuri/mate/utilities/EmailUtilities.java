package com.naimuri.mate.utilities;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Logger;

import javax.mail.*;
import javax.mail.internet.MimeUtility;
import javax.mail.search.SubjectTerm;

public class EmailUtilities {

    private static Logger log = Logger.getLogger("myLogger");

    public String getGmailMessageContainingSpecifiedHeaderText(String email, String password) throws Exception {
        Properties prop = new Properties();
        prop.setProperty("mail.imap.ssl.enable", "true");
        prop.setProperty("mail.store.protocol", "imaps");

        Session session = Session.getInstance(prop);
        Store store = session.getStore("imap");
        store.connect("imap.gmail.com", email, password);

        Folder folder = store.getFolder("INBOX");
        folder.open(Folder.READ_WRITE);



        Message[] messages = null;
        boolean isMailFound = false;
        Message mailFromProx = null;

        String mailText = "";
        // Search for mail from Prox with Subject = 'Welcome to aura'
        for (int i = 0; i <= 20; i++) {
            System.out.println("Total Message:" + folder.getMessageCount());
            System.out.println("Unread Message:" + folder.getUnreadMessageCount());
            if (folder.getUnreadMessageCount() == 0) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {

                }
            } else {
                i = 20;
            }
        }

        for (int i = 0; i <= 20; i++) {
            messages = folder.search(new SubjectTerm("Welcome to aura"), folder.getMessages());
            // Wait for up to 20 seconds

            if (messages != null) {
                for (Message mail : messages) {
                    if (!mail.isSet(Flags.Flag.SEEN)) {
                        mailFromProx = mail;
                        log.info("Message Count is: " + mailFromProx.getMessageNumber());
                        isMailFound = true;
                    }
                }

            } else {
                log.info("Message not found");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {

                }
            }

            //if unread message from aura in inbox proceed otherwise wait for a minute.
            if (isMailFound) {
                i = 20;
                log.info("Login message found");
                String line;
                StringBuffer buffer = new StringBuffer();

                try {
                    InputStream decodedStream = MimeUtility.decode(mailFromProx.getInputStream(), "quoted-printable");
                    try (Scanner scanner = new Scanner(decodedStream, StandardCharsets.UTF_8.name())) {
                        mailText = scanner.useDelimiter("\\A").next();
                    }
                }
                catch (MessagingException e) {
                    e.printStackTrace();
                }
            } else {
                log.info("Login message not found");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {

                }
            }


        }

        return mailText;
    }


}
